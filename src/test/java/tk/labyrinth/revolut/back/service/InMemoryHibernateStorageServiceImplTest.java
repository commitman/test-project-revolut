package tk.labyrinth.revolut.back.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.LockAcquisitionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.revolut.common.model.Account;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
class InMemoryHibernateStorageServiceImplTest {

	@Test
	void storeRetrieveUpdate() {
		StorageService storageService = new InMemoryHibernateStorageServiceImpl();
		UUID uid = UUID.randomUUID();
		{
			Account createdAccount = new Account(new BigDecimal("0.0000"), null, uid, null);
			storageService.store(createdAccount);
			Account retrievedAccount = storageService.retrieve(Account.class, uid);
			//
			Assertions.assertNotNull(retrievedAccount);
			Assertions.assertNotSame(createdAccount, retrievedAccount);
			Assertions.assertEquals(createdAccount, retrievedAccount);
		}
		{
			Account retrievedAccount = storageService.retrieve(Account.class, uid);
			Assertions.assertNotNull(retrievedAccount);
			storageService.store(retrievedAccount.withBalance(new BigDecimal("10.0000")));
			Account updatedAccount = storageService.retrieve(Account.class, uid);
			//
			Assertions.assertNotNull(updatedAccount);
			Assertions.assertEquals(new BigDecimal("10.0000"), updatedAccount.getBalance());
		}
	}

	@Test
	void transactionDeadlock() throws InterruptedException {
		StorageService storageService = new InMemoryHibernateStorageServiceImpl();
		UUID uid0 = UUID.randomUUID();
		UUID uid1 = UUID.randomUUID();
		storageService.store(Account.builder().uid(uid0).build());
		storageService.store(Account.builder().uid(uid1).build());
		//
		CountDownLatch primaryLatch = new CountDownLatch(2);
		CountDownLatch secondaryLatch = new CountDownLatch(2);
		AtomicBoolean deadlocked = new AtomicBoolean(false);
		ForkJoinPool.commonPool().execute(() -> storageService.withinTransaction(() -> {
			storageService.retrieveLocked(Account.class, uid0);
			primaryLatch.countDown();
			try {
				Assertions.assertTrue(primaryLatch.await(100, TimeUnit.MILLISECONDS));
			} catch (InterruptedException ex) {
				throw new RuntimeException(ex);
			}
			try {
				storageService.retrieveLocked(Account.class, uid1);
			} catch (LockAcquisitionException ex) {
				deadlocked.set(true);
			} finally {
				secondaryLatch.countDown();
			}
			return null;
		}));
		ForkJoinPool.commonPool().execute(() -> storageService.withinTransaction(() -> {
			storageService.retrieveLocked(Account.class, uid1);
			primaryLatch.countDown();
			try {
				Assertions.assertTrue(primaryLatch.await(100, TimeUnit.MILLISECONDS));
			} catch (InterruptedException ex) {
				throw new RuntimeException(ex);
			}
			try {
				storageService.retrieveLocked(Account.class, uid0);
			} catch (LockAcquisitionException ex) {
				deadlocked.set(true);
			} finally {
				secondaryLatch.countDown();
			}
			return null;
		}));
		{
			Assertions.assertTrue(secondaryLatch.await(100, TimeUnit.MILLISECONDS));
			Assertions.assertTrue(deadlocked.get());
			// Both Transactions ended, Deadlock happened -> success.
		}
	}

	/**
	 * TODO: Change sleep to latches.
	 */
	@Test
	void transactionLockAndOutsideStore() throws InterruptedException {
		StorageService storageService = new InMemoryHibernateStorageServiceImpl();
		UUID uid0 = UUID.randomUUID();
		UUID uid1 = UUID.randomUUID();
		storageService.store(Account.builder().balance(new BigDecimal("0.0000")).uid(uid0).build());
		storageService.store(Account.builder().balance(new BigDecimal("0.0000")).uid(uid1).build());
		//
		CountDownLatch primaryLatch = new CountDownLatch(1);
		{
			ForkJoinPool.commonPool().execute(() -> storageService.withinTransaction(() -> {
				Account retrievedAccount = storageService.retrieveLocked(Account.class, uid0);
				Assertions.assertNotNull(retrievedAccount);
				primaryLatch.countDown();
				try {
					Thread.sleep(200);
				} catch (InterruptedException ex) {
					throw new RuntimeException(ex);
				}
				return null;
			}));
		}
		Assertions.assertTrue(primaryLatch.await(100, TimeUnit.MILLISECONDS));
		CountDownLatch secondaryLatch = new CountDownLatch(2);
		{
			ForkJoinPool.commonPool().execute(() -> {
				// To be executed after Transaction above is ended.
				storageService.store(Account.builder()
						.balance(new BigDecimal("10.00"))
						.uid(uid0)
						.build());
				secondaryLatch.countDown();
			});
			ForkJoinPool.commonPool().execute(() -> {
				// To be executed immediately.
				storageService.store(Account.builder()
						.balance(new BigDecimal("20.00"))
						.uid(uid1)
						.build());
				secondaryLatch.countDown();
			});
		}
		{
			Assertions.assertFalse(secondaryLatch.await(150, TimeUnit.MILLISECONDS));
			{
				Account retrievedAccount0 = storageService.retrieve(Account.class, uid0);
				Assertions.assertNotNull(retrievedAccount0);
				Assertions.assertEquals(new BigDecimal("0.0000"), retrievedAccount0.getBalance());
			}
			{
				Account retrievedAccount1 = storageService.retrieve(Account.class, uid1);
				Assertions.assertNotNull(retrievedAccount1);
				Assertions.assertEquals(new BigDecimal("20.0000"), retrievedAccount1.getBalance());
			}
		}
		{
			Assertions.assertTrue(secondaryLatch.await(150, TimeUnit.MILLISECONDS));
			//
			Account retrievedAccount0 = storageService.retrieve(Account.class, uid0);
			Assertions.assertNotNull(retrievedAccount0);
			Assertions.assertEquals(new BigDecimal("10.0000"), retrievedAccount0.getBalance());
		}
	}

	@Test
	void transactionMultilock() throws InterruptedException {
		StorageService storageService = new InMemoryHibernateStorageServiceImpl();
		UUID uid = UUID.randomUUID();
		storageService.store(Account.builder().uid(uid).build());
		//
		CountDownLatch firstLockedlatch = new CountDownLatch(1);
		CountDownLatch secondTimedOutLatch = new CountDownLatch(1);
		//
		ForkJoinPool.commonPool().execute(() -> storageService.withinTransaction(() -> {
			storageService.retrieveLocked(Account.class, uid);
			firstLockedlatch.countDown();
			try {
				secondTimedOutLatch.await();
			} catch (InterruptedException ex) {
				throw new RuntimeException(ex);
			}
			return null;
		}));
		{
			Assertions.assertTrue(firstLockedlatch.await(100, TimeUnit.MILLISECONDS));
			// First acquired Lock -> run Second.
		}
		CountDownLatch secondLockedLatch = new CountDownLatch(1);
		ForkJoinPool.commonPool().execute(() -> storageService.withinTransaction(() -> {
			storageService.retrieveLocked(Account.class, uid);
			secondLockedLatch.countDown();
			return null;
		}));
		{
			Assertions.assertFalse(secondLockedLatch.await(100, TimeUnit.MILLISECONDS));
			// Second did not acquired Lock -> release First.
		}
		secondTimedOutLatch.countDown();
		{
			Assertions.assertTrue(secondLockedLatch.await(100, TimeUnit.MILLISECONDS));
			// Second acquired Lock -> success.
		}
	}

	@Test
	void transactionStoreRetrieveUpdate() {
		StorageService storageService = new InMemoryHibernateStorageServiceImpl();
		UUID uid = UUID.randomUUID();
		{
			Account createdAccount = new Account(new BigDecimal("0.0000"), null, uid, null);
			storageService.store(createdAccount);
			//
			storageService.withinTransaction(() -> {
				Account retrievedAccount = storageService.retrieve(Account.class, uid);
				//
				Assertions.assertNotNull(retrievedAccount);
				Assertions.assertNotSame(createdAccount, retrievedAccount);
				//
				Account retrievedAgainAccount = storageService.retrieve(Account.class, uid);
				//
				Assertions.assertNotNull(retrievedAgainAccount);
				Assertions.assertSame(retrievedAccount, retrievedAgainAccount);
				//
				retrievedAgainAccount.setBalance(new BigDecimal("10.0000"));
				return null;
			});
		}
		{
			Account retrievedAccount = storageService.retrieve(Account.class, uid);
			//
			Assertions.assertNotNull(retrievedAccount);
			Assertions.assertEquals(new BigDecimal("10.0000"), retrievedAccount.getBalance());
		}
	}
}
