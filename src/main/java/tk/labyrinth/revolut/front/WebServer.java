package tk.labyrinth.revolut.front;

import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Spark;
import tk.labyrinth.revolut.front.route.Router;

import javax.inject.Inject;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicBoolean;

public class WebServer {

	private final AtomicBoolean started = new AtomicBoolean(false);

	private final ObjectMapper objectMapper;

	private final List<Router> routers;

	@Inject
	public WebServer(ObjectMapper objectMapper, List<Router> routers) {
		this.objectMapper = objectMapper;
		this.routers = routers;
	}

	public void start() {
		if (started.compareAndSet(false, true)) {
			Spark.defaultResponseTransformer(objectMapper::writeValueAsString);
			Spark.get("healthcheck", (request, response) -> "Healthy at: " + Instant.now());
			Spark.get("shutdown", (request, response) -> {
				ForkJoinPool.commonPool().execute(this::stop);
				return "Shutting down at: " + Instant.now();
			});
			routers.forEach(Router::register);
		} else {
			throw new IllegalStateException("Already started");
		}
	}

	public void stop() {
		Spark.stop();
		Spark.defaultResponseTransformer(null);
		started.set(false);
	}
}
