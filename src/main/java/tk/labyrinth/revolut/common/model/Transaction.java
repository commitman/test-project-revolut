package tk.labyrinth.revolut.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@Data
@Entity
@NoArgsConstructor
public class Transaction {

	@Column(precision = 13, scale = 4)
	private BigDecimal amount;

	private UUID creditAccountUid;

	private UUID debitAccountUid;

	private Instant executedAt;

	@Id
	private UUID uid;
}
