package tk.labyrinth.revolut.back.service;

import tk.labyrinth.revolut.common.model.Account;
import tk.labyrinth.revolut.common.model.Transaction;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class SingleThreadTransactionProcessorImpl implements TransactionProcessor {

	private final ExecutorService executorService = Executors.newSingleThreadExecutor();

	private final StorageService storageService;

	@Inject
	public SingleThreadTransactionProcessorImpl(StorageService storageService) {
		this.storageService = storageService;
	}

	private TransactionResult doProcess(TransactionRequest request) {
		TransactionResult result;
		{
			Account creditAccount = storageService.retrieve(Account.class, request.getCreditAccountUid());
			if (creditAccount != null) {
				if (creditAccount.getBalance().compareTo(request.getAmount()) >= 0) {
					Account debitAccount = storageService.retrieve(Account.class, request.getDebitAccountUid());
					if (debitAccount != null) {
						Instant now = Instant.now();
						storeOutcome(creditAccount, debitAccount, request.getAmount(), now);
						result = TransactionResult.ofSuccess(request, now);
					} else {
						result = TransactionResult.ofFault(request, "Invalid debitAccountUid: " + request.getDebitAccountUid());
					}
				} else {
					result = TransactionResult.ofFault(request, "Not enough money");
				}
			} else {
				result = TransactionResult.ofFault(request, "Invalid creditAccountUid: " + request.getCreditAccountUid());
			}
		}
		return result;
	}

	private void storeOutcome(Account creditAccount, Account debitAccount, BigDecimal amount, Instant executedAt) {
		Account creditAccountToStore = creditAccount.toBuilder()
				.balance(creditAccount.getBalance().subtract(amount))
				.updatedAt(executedAt)
				.build();
		Account debitAccountToStore = debitAccount.toBuilder()
				.balance(debitAccount.getBalance().add(amount))
				.updatedAt(executedAt)
				.build();
		storageService.withinTransaction(() -> {
			storageService.store(new Transaction(amount, creditAccount.getUid(), debitAccount.getUid(), executedAt, UUID.randomUUID()));
			storageService.store(creditAccountToStore);
			storageService.store(debitAccountToStore);
			return null;
		});
	}

	@Override
	public TransactionResult process(TransactionRequest request) {
		try {
			return executorService.submit(() -> doProcess(request)).get();
		} catch (ExecutionException | InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
}
