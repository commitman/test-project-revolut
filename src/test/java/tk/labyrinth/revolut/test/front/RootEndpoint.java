package tk.labyrinth.revolut.test.front;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RootEndpoint {

	@GET("healthcheck")
	Call<String> healthcheck();
}
