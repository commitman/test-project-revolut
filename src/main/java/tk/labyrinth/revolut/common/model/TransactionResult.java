package tk.labyrinth.revolut.common.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.annotation.Nullable;
import java.time.Instant;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Data
public class TransactionResult {

	@Nullable
	private Instant executedAt;

	@Nullable
	private String faultMessage;

	private TransactionRequest request;

	public boolean calcExecuted() {
		return executedAt != null;
	}

	public static TransactionResult ofFault(TransactionRequest request, String faultMessage) {
		return new TransactionResult(null, faultMessage, request);
	}

	public static TransactionResult ofSuccess(TransactionRequest request, Instant executedAt) {
		return new TransactionResult(executedAt, null, request);
	}
}
