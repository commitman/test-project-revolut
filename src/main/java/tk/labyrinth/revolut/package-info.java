@PackageLevelNonnull
@Propagate(PackageLevelNonnull.class)
package tk.labyrinth.revolut;

import tk.labyrinth.javapig.Propagate;
import tk.labyrinth.misc4j.codestyle.PackageLevelNonnull;
