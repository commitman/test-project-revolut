package tk.labyrinth.revolut.common.service;

import tk.labyrinth.revolut.common.model.Account;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

public interface AccountService {

	Account create(Account account);

	@Nullable
	Account get(UUID uid);

	List<Account> list();
}
