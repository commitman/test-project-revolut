package tk.labyrinth.revolut.test.front;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import tk.labyrinth.revolut.common.model.Account;

import java.util.List;

public interface AccountEndpoint {

	@POST("accounts")
	Call<Account> create(@Body Account body);

	@GET("accounts")
	Call<List<Account>> list();
}
