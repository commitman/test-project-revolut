package tk.labyrinth.revolut.back.service;

import tk.labyrinth.revolut.common.model.Transaction;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;
import tk.labyrinth.revolut.common.service.TransactionService;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class TransactionServiceImpl implements TransactionService {

	private final StorageService storageService;

	private final TransactionProcessor transactionProcessor;

	@Inject
	public TransactionServiceImpl(StorageService storageService, TransactionProcessor transactionProcessor) {
		this.storageService = storageService;
		this.transactionProcessor = transactionProcessor;
	}

	@Override
	public TransactionResult create(TransactionRequest request) {
		TransactionResult result;
		if (request.getCreditAccountUid() == null) {
			result = TransactionResult.ofFault(request, "Require non-null: creditAccountUid");
		} else if (request.getDebitAccountUid() == null) {
			result = TransactionResult.ofFault(request, "Require non-null: debitAccountUid");
		} else if (Objects.equals(request.getCreditAccountUid(), request.getDebitAccountUid())) {
			result = TransactionResult.ofFault(request, "Require not equal: accountUids: " + request.getCreditAccountUid());
		} else if (request.getAmount() == null || request.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			result = TransactionResult.ofFault(request, "Require positive: amount: " + request.getAmount());
		} else {
			result = transactionProcessor.process(request);
		}
		return result;
	}

	@Nullable
	@Override
	public Transaction get(UUID uid) {
		Objects.requireNonNull(uid, "uid");
		return storageService.retrieve(Transaction.class, uid);
	}

	@Override
	public List<Transaction> list() {
		return storageService.searchAll(Transaction.class);
	}
}
