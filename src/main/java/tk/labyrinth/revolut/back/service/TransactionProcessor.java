package tk.labyrinth.revolut.back.service;

import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;

public interface TransactionProcessor {

	TransactionResult process(TransactionRequest request);
}
