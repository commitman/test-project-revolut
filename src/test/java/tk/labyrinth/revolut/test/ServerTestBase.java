package tk.labyrinth.revolut.test;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import tk.labyrinth.revolut.ApplicationComponent;
import tk.labyrinth.revolut.DaggerApplicationComponent;
import tk.labyrinth.revolut.test.front.AccountEndpoint;
import tk.labyrinth.revolut.test.front.RootEndpoint;
import tk.labyrinth.revolut.test.front.TransactionEndpoint;

@Accessors(fluent = true)
public class ServerTestBase {

	@Getter
	private final AccountEndpoint accountEndpoint;

	@Getter
	private final RootEndpoint rootEndpoint;

	@Getter
	private final TransactionEndpoint transactionEndpoint;

	private ApplicationComponent applicationComponent = DaggerApplicationComponent.create();

	{
		Retrofit.Builder builder = new Retrofit.Builder()
				.baseUrl("http://localhost:4567");
		{
			Retrofit jacksonRetrofit = builder
					.addConverterFactory(JacksonConverterFactory.create(applicationComponent.objectMapper()))
					.build();
			accountEndpoint = jacksonRetrofit.create(AccountEndpoint.class);
			transactionEndpoint = jacksonRetrofit.create(TransactionEndpoint.class);
		}
		{
			rootEndpoint = null;
		}
	}

	@AfterEach
	private void afterEach() {
		applicationComponent.webServer().stop();
	}

	@BeforeEach
	private void beforeEach() {
		applicationComponent = DaggerApplicationComponent.create();
		applicationComponent.webServer().start();
	}
}
