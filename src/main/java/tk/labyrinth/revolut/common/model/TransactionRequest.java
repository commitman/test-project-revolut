package tk.labyrinth.revolut.common.model;

import lombok.Value;

import java.math.BigDecimal;
import java.util.UUID;

@Value
public class TransactionRequest {

	BigDecimal amount;

	UUID creditAccountUid;

	UUID debitAccountUid;
}
