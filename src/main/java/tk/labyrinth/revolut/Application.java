package tk.labyrinth.revolut;

public class Application {

	public static void main(String... args) {
		DaggerApplicationComponent.create().webServer().start();
	}
}
