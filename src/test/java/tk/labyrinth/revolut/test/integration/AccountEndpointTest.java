package tk.labyrinth.revolut.test.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.revolut.common.model.Account;
import tk.labyrinth.revolut.test.ServerTestBase;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;

class AccountEndpointTest extends ServerTestBase {

	@Test
	void create() throws IOException {
		{
			// Create without balance.
			Account account = accountEndpoint().create(new Account()).execute().body();
			Assertions.assertNotNull(account);
			Assertions.assertEquals(BigDecimal.ZERO, account.getBalance());
			Assertions.assertNotNull(account.getUid());
			Assertions.assertTrue(Duration.between(account.getCreatedAt(), Instant.now())
					.compareTo(Duration.ofSeconds(1)) < 0);
			Assertions.assertEquals(account.getCreatedAt(), account.getUpdatedAt());
		}
		{
			// Create with balance.
			Account account = accountEndpoint().create(new Account().withBalance(new BigDecimal("12.34"))).execute().body();
			Assertions.assertNotNull(account);
			Assertions.assertEquals(new BigDecimal("12.34"), account.getBalance());
			Assertions.assertNotNull(account.getUid());
			Assertions.assertTrue(Duration.between(account.getCreatedAt(), Instant.now())
					.compareTo(Duration.ofSeconds(1)) < 0);
			Assertions.assertEquals(account.getCreatedAt(), account.getUpdatedAt());
		}
	}

	@Test
	void list() throws IOException {
		accountEndpoint().create(Account.builder().balance(new BigDecimal("10")).build()).execute();
		accountEndpoint().create(Account.builder().balance(new BigDecimal("20")).build()).execute();
		//
		List<Account> accounts = accountEndpoint().list().execute().body();
		Assertions.assertNotNull(accounts);
		Assertions.assertEquals(2, accounts.size());
		Assertions.assertNotNull(accounts.get(0).getCreatedAt());
		Assertions.assertNotNull(accounts.get(1).getCreatedAt());
		accounts.sort(Comparator.comparing(Account::getCreatedAt));
		Assertions.assertEquals(new BigDecimal("10.0000"), accounts.get(0).getBalance());
		Assertions.assertEquals(new BigDecimal("20.0000"), accounts.get(1).getBalance());
	}
}
