package tk.labyrinth.revolut;

import com.fasterxml.jackson.databind.ObjectMapper;
import dagger.Component;
import tk.labyrinth.revolut.back.BackModule;
import tk.labyrinth.revolut.front.FrontModule;
import tk.labyrinth.revolut.front.WebServer;

import javax.inject.Singleton;

@Component(modules = {BackModule.class, FrontModule.class})
@Singleton
public interface ApplicationComponent {

	ObjectMapper objectMapper();

	WebServer webServer();
}
