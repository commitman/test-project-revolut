package tk.labyrinth.revolut.test.front;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import tk.labyrinth.revolut.common.model.Transaction;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;

import java.util.List;

public interface TransactionEndpoint {

	@POST("transactions")
	Call<TransactionResult> create(@Body TransactionRequest body);

	@GET("transactions")
	Call<List<Transaction>> list();
}
