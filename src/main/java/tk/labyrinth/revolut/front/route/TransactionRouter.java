package tk.labyrinth.revolut.front.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import spark.Spark;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.service.TransactionService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@Slf4j
public class TransactionRouter extends RouterBase {

	private final TransactionService transactionService;

	@Inject
	public TransactionRouter(ObjectMapper objectMapper, TransactionService transactionService) {
		super(objectMapper);
		this.transactionService = transactionService;
	}

	@Override
	public void register() {
		Spark.path("transactions", () -> {
			Spark.get("", (request, response) ->
					transactionService.list());
			Spark.get("/:uid", (request, response) ->
					transactionService.get(UUID.fromString(request.params(":uid"))));
			Spark.post("", (request, response) ->
					transactionService.create(parseRequestBody(request, TransactionRequest.class)));
		});
	}
}
