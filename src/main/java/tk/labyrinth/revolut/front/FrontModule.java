package tk.labyrinth.revolut.front;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import dagger.Module;
import dagger.Provides;
import tk.labyrinth.revolut.front.route.AccountRouter;
import tk.labyrinth.revolut.front.route.Router;
import tk.labyrinth.revolut.front.route.TransactionRouter;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;

@Module
public class FrontModule {

	@Provides
	@Singleton
	public ObjectMapper objectMapper() {
		return new ObjectMapper()
				.registerModule(new JavaTimeModule())
				.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
				.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
	}

	@Inject
	@Provides
	public List<Router> routers(AccountRouter accountRouter, TransactionRouter transactionRouter) {
		return Arrays.asList(accountRouter, transactionRouter);
	}
}
