package tk.labyrinth.revolut.test.integration;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.revolut.common.model.Account;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;
import tk.labyrinth.revolut.test.ServerTestBase;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
public class Simulation extends ServerTestBase {

	private final Random random = new Random();

	private Map<UUID, BigDecimal> calculateExpectedAccountBalances(List<Account> accounts, List<TransactionResult> transactionResults) {
		Map<UUID, BigDecimal> result = new HashMap<>();
		accounts.forEach(account -> result.put(account.getUid(), account.getBalance()));
		transactionResults.forEach(transactionResult -> {
			if (transactionResult.getExecutedAt() != null) {
				result.compute(transactionResult.getRequest().getCreditAccountUid(), (key, value) -> {
					Objects.requireNonNull(value);
					return value.subtract(transactionResult.getRequest().getAmount());
				});
				result.compute(transactionResult.getRequest().getDebitAccountUid(), (key, value) -> {
					Objects.requireNonNull(value);
					return value.add(transactionResult.getRequest().getAmount());
				});
			}
		});
		return result;
	}

	private Stream<Account> createAccounts(int number) {
		return IntStream.range(0, number).mapToObj(index -> {
			try {
				return accountEndpoint().create(Account.builder()
						.balance(generateBigDecimal(1000, 10000))
						.build()).execute().body();
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		});
	}

	private Stream<TransactionRequest> createTransactionRequests(List<Account> accounts, int number, int maxAmount) {
		int accountCount = accounts.size();
		return IntStream.range(0, number).mapToObj(index -> {
			int creditAccountIndex = random.nextInt(accountCount);
			int debitAccountIndex;
			{
				int potentialDebitAccountIndex = random.nextInt(accountCount - 1);
				debitAccountIndex = potentialDebitAccountIndex != creditAccountIndex
						? potentialDebitAccountIndex
						: creditAccountIndex + 1;
			}
			return new TransactionRequest(
					generateBigDecimal(1, maxAmount),
					accounts.get(creditAccountIndex).getUid(),
					accounts.get(debitAccountIndex).getUid());
		});
	}

	private BigDecimal generateBigDecimal(int from, int to) {
		return new BigDecimal(from + random.nextDouble() * (to - from)).setScale(4, RoundingMode.HALF_EVEN);
	}

	private List<Account> getAccounts() {
		try {
			return accountEndpoint().list().execute().body();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	private TransactionResult sendTransactionRequest(TransactionRequest transactionRequest) {
		try {
			return transactionEndpoint().create(transactionRequest).execute().body();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	private List<TransactionResult> sendTransactionRequests(Stream<TransactionRequest> transactionRequests, boolean parallel) {
		Stream<TransactionRequest> transactionRequestStream = parallel
				? transactionRequests.parallel()
				: transactionRequests.sequential();
		return transactionRequestStream.map(this::sendTransactionRequest).collect(Collectors.toList());
	}

	@Test
	void oneTransaction() {
		int accountCount = 2;
		int transactionCount = 1;
		int maxAmount = 1000;
		//
		List<Account> accounts = createAccounts(accountCount).collect(Collectors.toList());
		List<TransactionRequest> transactionRequests = createTransactionRequests(accounts, transactionCount, maxAmount)
				.collect(Collectors.toList());
		//
		logger.info("Sending {} Transactions", transactionRequests.size());
		List<TransactionResult> transactionResults = sendTransactionRequests(transactionRequests.stream(), false);
		//
		Assertions.assertEquals(transactionCount, transactionResults.stream().filter(TransactionResult::calcExecuted).count());
		//
		logger.info("Checking Account Balances");
		Map<UUID, BigDecimal> expectedAccountBalances = calculateExpectedAccountBalances(accounts, transactionResults);
		List<Account> actualAccounts = getAccounts();
		//
		Assertions.assertEquals(expectedAccountBalances, actualAccounts.stream()
				.collect(Collectors.toMap(Account::getUid, Account::getBalance)));
	}

	@Test
	void tenTransactionsParallel() {
		int accountCount = 2;
		int transactionCount = 10;
		int maxAmount = 100;
		//
		List<Account> accounts = createAccounts(accountCount).collect(Collectors.toList());
		List<TransactionRequest> transactionRequests = createTransactionRequests(accounts, transactionCount, maxAmount)
				.collect(Collectors.toList());
		//
		logger.info("Sending Transactions: {}", transactionRequests.size());
		List<TransactionResult> transactionResults = sendTransactionRequests(transactionRequests.stream(), true);
		//
		Assertions.assertEquals(transactionCount, transactionResults.stream().filter(TransactionResult::calcExecuted).count());
		//
		logger.info("Checking Account Balances");
		Map<UUID, BigDecimal> expectedAccountBalances = calculateExpectedAccountBalances(accounts, transactionResults);
		List<Account> actualAccounts = getAccounts();
		//
		Assertions.assertEquals(expectedAccountBalances, actualAccounts.stream()
				.collect(Collectors.toMap(Account::getUid, Account::getBalance)));
	}

	@Test
	void thousandTransactionsParallel() {
		int accountCount = 50;
		int transactionCount = 1000;
		int maxAmount = 1500;
		//
		List<Account> accounts = createAccounts(accountCount).collect(Collectors.toList());
		List<TransactionRequest> transactionRequests = createTransactionRequests(accounts, transactionCount, maxAmount)
				.collect(Collectors.toList());
		//
		logger.info("Sending Transactions: {}", transactionRequests.size());
		long startedSendingAt = System.currentTimeMillis();
		List<TransactionResult> transactionResults = sendTransactionRequests(transactionRequests.stream(), true);
		//
		logger.info("Executed Transactions: count = {}/{}, tps = {}",
				transactionResults.stream().filter(TransactionResult::calcExecuted).count(),
				transactionCount,
				(float) (transactionCount / ((System.currentTimeMillis() - startedSendingAt) / 1000d)));
		logger.info("Checking Account Balances");
		Map<UUID, BigDecimal> expectedAccountBalances = calculateExpectedAccountBalances(accounts, transactionResults);
		List<Account> actualAccounts = getAccounts();
		//
		Assertions.assertEquals(expectedAccountBalances, actualAccounts.stream()
				.collect(Collectors.toMap(Account::getUid, Account::getBalance)));
	}

	@Test
	void thousandTransactionsSequential() {
		int accountCount = 50;
		int transactionCount = 1000;
		int maxAmount = 1500;
		//
		List<Account> accounts = createAccounts(accountCount).collect(Collectors.toList());
		List<TransactionRequest> transactionRequests = createTransactionRequests(accounts, transactionCount, maxAmount)
				.collect(Collectors.toList());
		//
		logger.info("Sending Transactions: {}", transactionRequests.size());
		long startedSendingAt = System.currentTimeMillis();
		List<TransactionResult> transactionResults = sendTransactionRequests(transactionRequests.stream(), false);
		//
		logger.info("Executed Transactions: count = {}/{}, tps = {}",
				transactionResults.stream().filter(TransactionResult::calcExecuted).count(),
				transactionCount,
				(float) (transactionCount / ((System.currentTimeMillis() - startedSendingAt) / 1000d)));
		logger.info("Checking Account Balances");
		Map<UUID, BigDecimal> expectedAccountBalances = calculateExpectedAccountBalances(accounts, transactionResults);
		List<Account> actualAccounts = getAccounts();
		//
		Assertions.assertEquals(expectedAccountBalances, actualAccounts.stream()
				.collect(Collectors.toMap(Account::getUid, Account::getBalance)));
	}
}
