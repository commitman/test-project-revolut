package tk.labyrinth.revolut.back.service;

import tk.labyrinth.revolut.common.model.Account;
import tk.labyrinth.revolut.common.service.AccountService;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Singleton
public class AccountServiceImpl implements AccountService {

	private final StorageService storageService;

	@Inject
	public AccountServiceImpl(StorageService storageService) {
		this.storageService = storageService;
	}

	@Override
	public Account create(Account account) {
		Account accountToStore;
		if (account.getCreatedAt() != null) {
			throw new IllegalArgumentException("Require no createdAt: " + account.getUid());
		}
		if (account.getUid() != null) {
			throw new IllegalArgumentException("Require no uid: " + account.getUid());
		}
		if (account.getUpdatedAt() != null) {
			throw new IllegalArgumentException("Require no updatedAt: " + account.getUid());
		}
		if (account.getBalance() != null) {
			if (account.getBalance().compareTo(BigDecimal.ZERO) < 0) {
				throw new IllegalArgumentException("Require non-negative: balance" + account.getBalance());
			}
			accountToStore = account;
		} else {
			accountToStore = account.withBalance(BigDecimal.ZERO);
		}
		{
			Instant now = Instant.now();
			accountToStore.setCreatedAt(now);
			accountToStore.setUid(UUID.randomUUID());
			accountToStore.setUpdatedAt(now);
		}
		storageService.store(accountToStore);
		return accountToStore;
	}

	@Nullable
	@Override
	public Account get(UUID uid) {
		Objects.requireNonNull(uid, "uid");
		return storageService.retrieve(Account.class, uid);
	}

	@Override
	public List<Account> list() {
		return storageService.searchAll(Account.class);
	}
}
