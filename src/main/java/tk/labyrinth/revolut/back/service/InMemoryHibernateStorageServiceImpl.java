package tk.labyrinth.revolut.back.service;

import lombok.extern.slf4j.Slf4j;
import org.h2.Driver;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.H2Dialect;
import tk.labyrinth.revolut.common.model.Account;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
public class InMemoryHibernateStorageServiceImpl implements StorageService {

	private final SessionFactory sessionFactory;

	private final ThreadLocal<Integer> transactionDepth = ThreadLocal.withInitial(() -> null);

	@Inject
	public InMemoryHibernateStorageServiceImpl() {
		Configuration configuration = new Configuration();
		{
			Properties properties = new Properties();
			properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
			properties.put(Environment.DIALECT, H2Dialect.class.getName());
			properties.put(Environment.DRIVER, Driver.class.getName());
			properties.put(Environment.HBM2DDL_AUTO, "create");
			properties.put(Environment.URL, "jdbc:h2:mem:testdb");
			configuration.addProperties(properties);
		}
		configuration.addPackage("tk.labyrinth.revolut.common.model");
		configuration.addAnnotatedClass(Account.class);
		configuration.addAnnotatedClass(tk.labyrinth.revolut.common.model.Transaction.class);
		sessionFactory = configuration.buildSessionFactory(new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties())
				.build());
	}

	private <T> T withinHibernateTransaction(Function<Session, T> action) {
		Session session = sessionFactory.getCurrentSession();
		Integer depth = transactionDepth.get();
		Transaction transaction;
		if (depth == null) {
			transaction = session.beginTransaction();
		} else {
			transaction = session.getTransaction();
		}
		boolean rollback = true;
		transactionDepth.set(depth == null ? 1 : depth + 1);
		try {
			T result = action.apply(session);
			if (depth == null) {
				transaction.commit();
				rollback = false;
			} else {
				rollback = false;
			}
			return result;
		} finally {
			transactionDepth.set(depth);
			if (rollback) {
				transaction.rollback();
			}
		}
	}

	@Nullable
	@Override
	public <T> T retrieve(Class<T> type, UUID uid) {
		return withinHibernateTransaction(session -> session.get(type, uid));
	}

	@Nullable
	@Override
	public <T> T retrieveLocked(Class<T> type, UUID uid) {
		return withinHibernateTransaction(session -> session.get(type, uid, LockMode.PESSIMISTIC_WRITE));
	}

	@Override
	@SuppressWarnings({"deprecation", "unchecked"})
	public <T> List<T> searchAll(Class<T> type) {
		{
			// TODO: Replace with pure JPA one day.
//			CriteriaQuery<T> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(type);
//			Root<T> root = criteriaQuery.from(type);
//			criteriaQuery.select(root);
//			return entityManager.createQuery(criteriaQuery).getResultList();
		}
		return withinHibernateTransaction(session -> session.createCriteria(type).list());
	}

	@Override
	public void store(Object value) {
		withinHibernateTransaction(session -> {
			session.saveOrUpdate(value);
			return null;
		});
	}

	@Override
	public <T> T withinTransaction(Supplier<T> action) {
		return withinHibernateTransaction(session -> action.get());
	}
}
