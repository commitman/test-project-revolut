package tk.labyrinth.revolut.back;

import dagger.Module;
import dagger.Provides;
import tk.labyrinth.revolut.back.service.AccountServiceImpl;
import tk.labyrinth.revolut.back.service.InMemoryHibernateStorageServiceImpl;
import tk.labyrinth.revolut.back.service.SqlBasedTransactionProcessorImpl;
import tk.labyrinth.revolut.back.service.StorageService;
import tk.labyrinth.revolut.back.service.TransactionProcessor;
import tk.labyrinth.revolut.back.service.TransactionServiceImpl;
import tk.labyrinth.revolut.common.service.AccountService;
import tk.labyrinth.revolut.common.service.TransactionService;

@Module
public class BackModule {

	@Provides
	public AccountService accountService(AccountServiceImpl accountService) {
		return accountService;
	}

	@Provides
	public StorageService storageService(InMemoryHibernateStorageServiceImpl storageService) {
		return storageService;
	}

	@Provides
	public TransactionProcessor transactionProcessor(SqlBasedTransactionProcessorImpl transactionProcessor) {
		return transactionProcessor;
	}

	@Provides
	public TransactionService transactionService(TransactionServiceImpl transactionService) {
		return transactionService;
	}
}
