package tk.labyrinth.revolut.back.service;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

public interface StorageService {

	@Nullable
	<T> T retrieve(Class<T> type, UUID uid);

	@Nullable
	<T> T retrieveLocked(Class<T> type, UUID uid);

	<T> List<T> searchAll(Class<T> type);

	void store(Object value);

	<T> T withinTransaction(Supplier<T> action);
}
