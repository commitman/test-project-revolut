package tk.labyrinth.revolut.front.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import spark.Spark;
import tk.labyrinth.revolut.common.model.Account;
import tk.labyrinth.revolut.common.service.AccountService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@Slf4j
public class AccountRouter extends RouterBase {

	private final AccountService accountService;

	@Inject
	public AccountRouter(ObjectMapper objectMapper, AccountService accountService) {
		super(objectMapper);
		this.accountService = accountService;
	}

	@Override
	public void register() {
		Spark.path("accounts", () -> {
			Spark.get("", (request, response) ->
					accountService.list());
			Spark.get("/:uid", (request, response) ->
					accountService.get(UUID.fromString(request.params(":uid"))));
			Spark.post("", (request, response) ->
					accountService.create(parseRequestBody(request, Account.class)));
		});
	}
}
