package tk.labyrinth.revolut.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@Builder(toBuilder = true)
@Data
@Entity
@NoArgsConstructor
@Wither
public class Account {

	@Column(precision = 13, scale = 4)
	private BigDecimal balance;

	private Instant createdAt;

	@Id
	private UUID uid;

	private Instant updatedAt;
}
