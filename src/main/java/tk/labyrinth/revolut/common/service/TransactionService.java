package tk.labyrinth.revolut.common.service;

import tk.labyrinth.revolut.common.model.Transaction;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

public interface TransactionService {

	TransactionResult create(TransactionRequest request);

	@Nullable
	Transaction get(UUID uid);

	List<Transaction> list();
}
