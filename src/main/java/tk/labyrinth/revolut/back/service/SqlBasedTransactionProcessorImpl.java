package tk.labyrinth.revolut.back.service;

import tk.labyrinth.revolut.common.model.Account;
import tk.labyrinth.revolut.common.model.Transaction;
import tk.labyrinth.revolut.common.model.TransactionRequest;
import tk.labyrinth.revolut.common.model.TransactionResult;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Singleton
public class SqlBasedTransactionProcessorImpl implements TransactionProcessor {

	private final StorageService storageService;

	@Inject
	public SqlBasedTransactionProcessorImpl(StorageService storageService) {
		this.storageService = storageService;
	}

	private TransactionResult doProcess(TransactionRequest request) {
		TransactionResult result;
		{
			Account creditAccount;
			Account debitAccount;
			{
				// Retrieving Accounts ascending by their uids to avoid Deadlocks.
				if (request.getCreditAccountUid().compareTo(request.getDebitAccountUid()) < 0) {
					creditAccount = storageService.retrieveLocked(Account.class, request.getCreditAccountUid());
					debitAccount = storageService.retrieveLocked(Account.class, request.getDebitAccountUid());
				} else {
					debitAccount = storageService.retrieveLocked(Account.class, request.getDebitAccountUid());
					creditAccount = storageService.retrieveLocked(Account.class, request.getCreditAccountUid());
				}
			}
			if (creditAccount != null) {
				if (creditAccount.getBalance().compareTo(request.getAmount()) >= 0) {
					if (debitAccount != null) {
						Instant now = Instant.now();
						storeOutcome(creditAccount, debitAccount, request.getAmount(), now);
						result = TransactionResult.ofSuccess(request, now);
					} else {
						result = TransactionResult.ofFault(request, "Invalid debitAccountUid: " + request.getDebitAccountUid());
					}
				} else {
					result = TransactionResult.ofFault(request, "Not enough money");
				}
			} else {
				result = TransactionResult.ofFault(request, "Invalid creditAccountUid: " + request.getCreditAccountUid());
			}
		}
		return result;
	}

	private void storeOutcome(Account creditAccount, Account debitAccount, BigDecimal amount, Instant executedAt) {
		creditAccount.setBalance(creditAccount.getBalance().subtract(amount));
		creditAccount.setUpdatedAt(executedAt);
		debitAccount.setBalance(debitAccount.getBalance().add(amount));
		debitAccount.setUpdatedAt(executedAt);
		storageService.store(new Transaction(amount, creditAccount.getUid(), debitAccount.getUid(), executedAt, UUID.randomUUID()));
	}

	@Override
	public TransactionResult process(TransactionRequest request) {
		return storageService.withinTransaction(() -> doProcess(request));
	}
}
