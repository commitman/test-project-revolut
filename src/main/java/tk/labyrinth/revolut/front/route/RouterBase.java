package tk.labyrinth.revolut.front.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;

import java.io.IOException;

public abstract class RouterBase implements Router {

	private final ObjectMapper objectMapper;

	public RouterBase(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	protected <T> T parseRequestBody(Request request, Class<T> type) {
		try {
			return objectMapper.readValue(request.body(), type);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
